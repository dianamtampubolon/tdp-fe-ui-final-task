import './assets/css/style.scss'

// assets image
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {

  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className='top'>
        <div className='navbar'>
          <a href="#"><b>EDTS</b> TDP Batch #2</a>
          {/* Float links to the right. Hide them on small screens */}
          <div>
            <a href="#">Projects</a>
            <a href="#">About</a>
            <a href="#">Contact</a>
          </div>
        </div>
      </div>

      {/* Header */}
      <header className='container' id="home">
        <div>
          <h1>
            <span>
              <b>EDTS</b>
            </span>
            <span>Architects</span>
          </h1>
        </div>
      </header>

      {/* Page content */}
      <div className='container'>
        {/* Project Section */}
        <div id="projects">
          <h3>Projects</h3>
          <div className='projects-wrap'>
            <div className='row-padding'>
              <div>
                <div className='label'>Summer House</div>
                <img src={house2} alt="House" />
              </div>
              <div>
                <div className='label'>Brick House</div>
                <img src={house3} alt="House" />
              </div>
              <div>
                <div className='label'>Renovated</div>
                <img src={house4} alt="House" />
              </div>
              <div>
                <div className='label'>Barn House</div>
                <img src={house5} alt="House" />
              </div>
              <div>
                <div className='label'>Summer House</div>
                <img src={house3} alt="House" />
              </div>
              <div>
                <div className='label'>Brick House</div>
                <img src={house2} alt="House" />
              </div>
              <div>
                <div className='label'>Renovated</div>
                <img src={house5} alt="House" />
              </div>
              <div>
                <div className='label'>Barn House</div>
                <img src={house4} alt="House" />
              </div>
            </div>
          </div>
        </div>

        {/* About Section */}
        <div id='about'>
          <h3>About</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
          <div className='row-padding'>
            <div >
              <img src={team1} alt="Jane" />
              <h3>Jane Doe</h3>
              <p className='position'>CEO &amp; Founder</p>
              <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
              <p><button>Contact</button></p>
            </div>
            <div >
              <img src={team2} alt="John" />
              <h3>John Doe</h3>
              <p className='position'>Architect</p>
              <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
              <p><button>Contact</button></p>
            </div>
            <div >
              <img src={team3} alt="Mike" />
              <h3>Mike Ross</h3>
              <p className='position'>Architect</p>
              <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
              <p><button>Contact</button></p>
            </div>
            <div >
              <img src={team4} alt="Dan" />
              <h3 className='position'>Dan Star</h3>
              <p>Architect</p>
              <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
              <p><button>Contact</button></p>
            </div>
          </div>
        </div>

        {/* Contact Section */}
        <div id="contact">
          <h3>Contact</h3>
          <p>Lets get in touch and talk about your next project.</p>

          <form>
            <input type="text" name="name" placeholder='Name' />
            <input type="text" name="email" placeholder="Email" />
            <input type="text" name="subject" placeholder='Subject' />
            <input type="text" name="comment" placeholder="Comment" />
            <input type="submit" value="Send Message" />
          </form> 

        </div>

        {/* Image of location/map */}
        <div id='location' className='container'>
          <img className='img-fluid' src={map} alt="maps"/>
        </div>

        <div id='contact-list'>
          <h3>Contact List</h3>

          <div>
            <table>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Country</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Alfreds Futterkiste</td>
                  <td>alfreds@gmail.com</td>
                  <td>Germany</td>
                </tr>
                <tr>
                  <td>Reza</td>
                  <td>reza@gmail.com</td>
                  <td>Germany</td>
                </tr>
                <tr>
                  <td>Ismail</td>
                  <td>ismail@gmail.com</td>
                  <td>Germany</td>
                </tr>
                <tr>
                  <td>Holland</td>
                  <td>holland@gmail.com</td>
                  <td>Germany</td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
